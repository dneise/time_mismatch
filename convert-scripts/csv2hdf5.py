#! /usr/bin/env python3
import json
import numpy as np
import h5py
from GetBoolRingPixel import get_bool_ringpixel

def array_to_dict(array, keys):
    params = {}
    for key, param in zip(keys, array):
        params[key] = param
    return params


if __name__ == '__main__':
    NewPSF_params = np.loadtxt("data/NewPSF_cut.csv", unpack=True)
    new_ProtonMC_params = np.loadtxt("data/ProtonMC2_cut.csv", unpack=True)

    mc_keys = ["Peakness", "Distance", "Percentage", "OctantsHit", "Size2", "Size3",
               "Size4", "Size6", "Size8", "Size10", "Leakage", "Leakage2", "NumPix2",
               "NumPix3", "NumPix4", "NumPix6", "NumPix8", "NumPix10", "MeanTimeRing2",
               "StdDevTimeRing2", "MeanTimeRing3", "StdDevTimeRing3", "MeanTimeRing4",
               "StdDevTimeRing4", "MeanTimeRing6", "StdDevTimeRing6", "MeanTimeRing8",
               "StdDevTimeRing8", "MeanTimeRing10", "StdDevTimeRing10", "BestR",
               "BestX", "BestY", "CorsikaZenit", "CorsikaAzimut", "CorsikaX", "CorsikaY",
               "CorsikaEnergy", "EventNum"
               ]

    data_keys = ["Peakness", "Distance", "Percentage", "OctantsHit", "Size2", "Size3",
                 "Size4", "Size6", "Size8", "Size10", "Leakage", "Leakage2", "NumPix2",
                 "NumPix3", "NumPix4", "NumPix6", "NumPix8", "NumPix10", "MeanTimeRing2",
                 "StdDevTimeRing2", "MeanTimeRing3", "StdDevTimeRing3", "MeanTimeRing4",
                 "StdDevTimeRing4", "MeanTimeRing6", "StdDevTimeRing6", "MeanTimeRing8",
                 "StdDevTimeRing8", "MeanTimeRing10", "StdDevTimeRing10", "BestR",
                 "BestX", "BestY","EventNum"
                 ]

    proton_mc = array_to_dict(new_ProtonMC_params, mc_keys)
    new_psf = array_to_dict(NewPSF_params, data_keys)

    proton_mc["photoncharge"] = np.genfromtxt("./data/ProtonMC2_photonCharge.csv")
    proton_mc["arrivalTime"] = np.genfromtxt("./data/ProtonMC2_arrivalTime.csv")
    proton_mc["bestRingPixel"] = get_bool_ringpixel("./data/ProtonMC2_ringPixel.csv")

    new_psf["photoncharge"] = np.genfromtxt("./data/NewPSF_photonCharge.csv")
    new_psf["arrivalTime"] = np.genfromtxt("./data/NewPSF_arrivalTime.csv")
    new_psf["bestRingPixel"] = get_bool_ringpixel("./data/NewPSF_ringPixel.csv")



    print("\nstart writing hdf5")
    outfile = h5py.File("data/new_psf.hdf5", "w")
    grp = outfile.create_group("facttools_output")
    size = 100
    for key in new_psf.keys():
        grp.create_dataset(key, data=new_psf[key], compression="gzip")
    outfile.close()

    outfile = h5py.File("data/proton_mc_20140610.hdf5", "w")
    grp = outfile.create_group("facttools_output")
    size = 100
    for key in proton_mc.keys():
        grp.create_dataset(key, data=proton_mc[key], compression="gzip")
    outfile.close()
