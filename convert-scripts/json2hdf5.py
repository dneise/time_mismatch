#! /usr/bin/env python3
"""
Usage:
    json2hdf5.py <inputfile> [<outputfile>]
"""

from docopt import docopt
import json
import numpy as np
import h5py

args = docopt(__doc__)

def get_keys(filename):
    with open(filename, 'r') as infile:
        event = json.loads(infile.readline())
        return list(event.keys())

def read_json(filename, keys):
    data = {}

    for key in keys:
        data[key] = []

    with open(filename, 'r') as infile:
        for line in infile:
            event = json.loads(line)
            for key in keys:
                data[key].append(event[key])
    for key in keys:
        data[key] = np.array(data[key])
    return data

def read_json_pixellist(filename, keys, pixelkeys):
    data = {}

    for key in keys:
        data[key] = []

    with open(filename, 'r') as infile:
            for line in infile:
                event = json.loads(line)
                for key in keys:
                    if key in pixelkeys:
                        pixel = np.zeros(1440).astype(bool)
                        pixel[event[key]] = True
                        data[key].append(pixel)
                    else:
                        data[key].append(event[key])
    for key in keys:
        data[key] = np.array(data[key])
    return data

if __name__ == '__main__':
    inputfile = args["<inputfile>"]
    if not args["<outputfile>"]:
        outputfile = inputfile.replace(".json", ".hdf5")
    else:
        outfile = args["<outputfile>"]

    data_keys = get_keys(inputfile)
    print("Keys in inputfile:")
    for key in sorted(data_keys):
        print(key)

    print("\nStart reading in json")
    data = read_json_pixellist(inputfile, data_keys, "bestRingPixel")
    print("\nFinished reading")

    print("\nstart writing hdf5")
    outfile = h5py.File(outputfile, "w")
    grp = outfile.create_group("facttools_output")

    size = 100
    for key in data:
        grp.create_dataset(key, data=data[key], compression="gzip")
    outfile.close()
    print("\nfinished writing hdf5")
