#! /usr/bin/env python2
""" This little  guy will read (hence the name) a bunch of files like
yyyymmdd_rrr.fits.fz.csv.gz, that typically fall out of the muon analysis by Max Noethe.
the folder structure is typically like this:
    ../base/yyyymmdd_rrr.fits.fz/yyyymmdd_rrr.fits.fz.csv.gz

this guy will simply loop over all those files and dump their contents
into one large HDF5 file, so it can be used for later analysis.
(Just because I hate playing with txt files.)

This script is pretty poorly written, improve it if you like ...
"""
import numpy as np
import matplotlib.pyplot as plt
import h5py
import glob
import gzip

filelist = glob.glob('AllPhidoFiles/*/*')
print "number of files to be read", len(filelist)

outfile = h5py.File("AllPhidoFiles.hdf5", "w")
grp = outfile.create_group("input")

size = 100
hPeak = grp.create_dataset("houghPeakness",   (size,), dtype='f32', maxshape=(None,))
hDist = grp.create_dataset("houghDistance",   (size,), dtype='f32', maxshape=(None,))
hPerc = grp.create_dataset("houghPercentage", (size,), dtype='f32', maxshape=(None,))
octHi = grp.create_dataset("octantsHit",      (size,), dtype='i8', maxshape=(None,))
bestR = grp.create_dataset("bestR",           (size,), dtype='f32', maxshape=(None,))
bestX = grp.create_dataset("bestX",           (size,), dtype='f32', maxshape=(None,))
bestY = grp.create_dataset("bestY",           (size,), dtype='f32', maxshape=(None,))
night = grp.create_dataset("NIGHT",           (size,), dtype='i32', maxshape=(None,))
runid = grp.create_dataset("RUNID",           (size,), dtype='i32', maxshape=(None,))
evNum = grp.create_dataset("EventNum",        (size,), dtype='i32', maxshape=(None,))
phoCh = grp.create_dataset("photonCharge",    (size, 1440), dtype='f32', maxshape=(None, 1440))
arrti = grp.create_dataset("arrivalTime",     (size, 1440), dtype='f32', maxshape=(None, 1440))
bripi = grp.create_dataset("bestRingPixel",   (size, 1440), dtype='?', maxshape=(None, 1440))

datasets = [ hPeak, hDist, hPerc, octHi, bestR, bestX, bestY,
            night, runid, evNum, phoCh, arrti, bripi, ]

tlc = 0 # total_line_counter

for file_id,path in enumerate(filelist):
    f = gzip.GzipFile(path)

    # get rid of headerline
    # houghPeakness/D:
    # houghDistance/D:
    # houghPercentage/D:
    # octantsHit/I:
    # bestR/D:
    # bestX/D:
    # bestY/D:
    # NIGHT/I:
    # RUNID/I:
    # EventNum/I:
    # photonCharge[1440]/D:
    # arrivalTime[1440]/I:
    # bestRingPixel[136]/I
    _ = f.readline()

    for line_id, line in enumerate(f):
        if line_id % 100 == 0:
            print file_id, '/', len(filelist), 'line:', line_id

        line = np.fromstring(line, dtype=float, sep=' ')

        if len(line) <= 2890:
            # if there are <= 2890 numbers in one line, then
            # no pixels survived the cuts.
            continue

        if tlc == size:
            for d in datasets:
                d.resize(size*2, axis=0)
            size *= 2

        hPeak[tlc] = line[0]
        hDist[tlc] = line[1]
        hPerc[tlc] = line[2]
        octHi[tlc] = line[3]
        bestR[tlc] = line[4]
        bestX[tlc] = line[5]
        bestY[tlc] = line[6]
        night[tlc] = line[7]
        runid[tlc] = line[8]
        evNum[tlc] = line[9]
        phoCh[tlc,:] = line[10:1450]
        arrti[tlc,:] = line[1450:2890]
        bripi[tlc,line[2890:]] = True

        tlc += 1

    print file_id, '/', len(filelist), 'total lines:', tlc

print "done. total number of lines:", tlc, "closing file."
for d in datasets:
    d.resize(tlc, axis=0)
outfile.close()
