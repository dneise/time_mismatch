#! /usr/bin/env python3
"""
This python script is intended to read input from
an hdf5-file containg facttools muon analysis output and calculate the
stddeviation of the arrivalTimes for pixel with a photoncharge higher
than 'threshold'
the calculated values are added as new dataset to the same group

Usage:
    calculate_sigma_arrivalTime.py <inputfile> [options]

Options:
    -g --group         input group of the hdf5file
    -t <threshold> --threshold=<threshold>  photoncharge threshold [default: 6.0]
    -d <delayfile> --delays=<delayfile>        calculate sigma_at witghh delay correction
"""


from docopt import docopt
import numpy as np
import h5py

args = docopt(__doc__)

print(args)

inputfile = args["<inputfile>"]
threshold = float(args["--threshold"])

print("Threshold:", threshold)

with h5py.File(inputfile, "r") as f:
    if not args["--group"]:
        group = list(f.keys())[0]
        print("Warning: no group specified, using ", group)
    else:
        group = args["--group"]

data = {}
print("start reading ...")
with h5py.File(inputfile, "r") as f:
    grp = f[group]
    n_keys = len(list(grp.keys()))
    for i,key in enumerate(grp.keys()):
        print("reading key {:3d} of {:3d} : ".format(i+1, n_keys), key)
        data[key] = np.array(grp[key])
    if args["--delays"] is not None:
        if args["--delays"].endswith("hdf5"):
            delayfile = h5py.File(args["--delays"], 'r')
            delays = np.array(delayfile["results/delay"])
            delayfile.close()
        elif args["--delays"].endswith(".txt") or args["--delays"].endswith(".csv"):
            delays = np.genfromtxt(args["--delays"], usecols=[0])
        else:
            raise IOError("Could not parse delay file, use hdf5, csv or txt")


data["numEvents"] = len(data["EventNum"])
print(data["numEvents"])

print("\nStart calculation")
data["StdDevTime{:d}".format(int(threshold))] = np.zeros(data["numEvents"])
for event in range(data["numEvents"]):
    mask = np.logical_and(data["photoncharge"][event]>=threshold, data["bestRingPixel"][event])
    signal_times = data["arrivalTime"][event][mask]
    data["StdDevTime{:d}".format(int(threshold))][event] = np.std(signal_times)


if args["--delays"] is not None:
    data["arrivalTimeCorr"] = np.zeros_like(data["arrivalTime"])
    data["StdDevTime{:d}Corr".format(int(threshold))] = np.zeros(data["numEvents"])
    for event in range(data["numEvents"]):
        data["arrivalTimeCorr"][event] = data["arrivalTime"][event] - delays
        mask = np.logical_and(data["photoncharge"][event]>=threshold, data["bestRingPixel"][event])
        signal_times = data["arrivalTimeCorr"][event][mask]
        data["StdDevTime{:d}Corr".format(int(threshold))][event] = np.std(signal_times)

print("\nStart writing")
with h5py.File(inputfile, "a") as f:
    grp = f[group]
    try:
        grp.create_dataset("StdDevTime{:d}".format(int(threshold)), data=data["StdDevTime{:d}".format(int(threshold))], compression="gzip")
    except:
        del grp["StdDevTime{:d}".format(int(threshold))]
        grp.create_dataset("StdDevTime{:d}".format(int(threshold)), data=data["StdDevTime{:d}".format(int(threshold))], compression="gzip")
    if args["--delays"] is not None:
        grp = f[group]
        try:
            grp.create_dataset("StdDevTime{:d}Corr".format(int(threshold)), data=data["StdDevTime{:d}Corr".format(int(threshold))], compression="gzip")
            grp.create_dataset("arrivalTimeCorr", data=data["arrivalTimeCorr"], compression="gzip")
        except:
            del grp["StdDevTime{:d}Corr".format(int(threshold))]
            del grp["arrivalTimeCorr"]
            grp.create_dataset("StdDevTime{:d}Corr".format(int(threshold)), data=data["StdDevTime{:d}Corr".format(int(threshold))], compression="gzip")
            grp.create_dataset("arrivalTimeCorr", data=data["arrivalTimeCorr"], compression="gzip")
