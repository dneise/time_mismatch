#!/usr/bin/env python3
"""
Idea: If you have the correct delays, the StandardDeviation, or
one operation less, the variance, is minimal. So we minimize
the variance to get the best fitting delays.

Current Problem: How do you deal with the fact, that we know
arrivaltimes just for a subset of pixels per event

Usage:
    calculate_delays_over_minimization.py <inputfile> <outputfile> [options]

Options:
    -g <group>     hdf5 group [default: facttools_output]
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from scipy.stats import sem  # standard error of the mean
import h5py
from progressbar import ProgressBar

from docopt import docopt

class RunningStats():
    def __init__(self):
        self.n = 0
        self._mean = 0
        self._delta = 0
        self._M2 = 0

    def add(self, data):
        self.n += 1
        self._delta = data - self._mean
        self._mean = self._mean + self._delta/self.n
        self._M2 = self._M2 + self._delta * (data - self._mean)

    def mean(self):
        return self._mean

    def var(self):
        if self.n < 2:
            raise ValueError("variance not defined for n<2")
        return self._M2/(self.n - 1)

    def std(self):
        return np.sqrt(self.var())

    def sem(self):
        return 1/self.n * self.std()


args = docopt(__doc__)

data = h5py.File(args["<inputfile>"]).get(args["-g"])
events = data["EventNum"].shape[0]

stats = RunningStats()

for event, arrivaltime in enumerate(data["arrivalTime"]):
    if event % 128 == 0:
        print("{} von {}".format(event, events), end='\r', flush=True)

    stats.add(arrivaltime - np.mean(arrivaltime))

np.savetxt(args["<outputfile>"],
        np.column_stack([stats.get_mean(), stats.get_sem()]),
        fmt="%1.6f",
        delimiter="\t",
        )
