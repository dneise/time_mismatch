#! /usr/bin/env python2
"""
Usage:
    do_cuts.py <inputfile> <outputfile> [options]

Options:
    --ingroup=<ingroup>  The input group [default: facttools_output]
    --outgroup=<outgroup>  The ouput group [default: cutted]
    --events=<events_per_run>  The numbers of events per run [default: 1000]
"""
from __future__ import print_function, unicode_literals, division
import h5py
import numpy as np
from docopt import docopt

args = docopt(__doc__)
print(args)

def do_cut(data, cutkey, func):
    mask = func(data[cutkey])
    for key in data.keys():
        data[key] = data[key][mask]



inputfile = h5py.File(args["<inputfile>"], 'r')
outputfile = h5py.File(args["<outputfile>"], 'w')
ingroup = inputfile[args["--ingroup"]]
outgroup = outputfile.create_group(args["--outgroup"])

keys = list(ingroup.keys())

for key in keys:
    if len(ingroup[key].shape) == 2:
        outgroup.create_dataset(key,
                                dtype=ingroup[key].dtype,
                                shape=(1, 1440),
                                maxshape=(None, 1440),
                                compression="gzip")
    else:
        outgroup.create_dataset(key,
                                dtype=ingroup[key].dtype,
                                shape=(1,),
                                maxshape=(None,),
                                compression="gzip")


NumEvents = ingroup[keys[0]].size
events_per_run = int(args["--events"])
runs = NumEvents//events_per_run +1

print("#Events:", NumEvents, "\n\n")
print("Events per run:", events_per_run, "\n\n")
surviving_events = 0
for i in range(runs):
    print("Run {:5d} of {:5d}".format(i+1, runs))
    low = i * events_per_run
    if i == runs:
        high = NumEvents
    else:
        high = (i+1) * events_per_run

    print(low)
    print(high)

    print("start reading")
    cur_data = {}
    for key in keys:
        cur_data[key] = np.array(ingroup[key][low:high])

    print("start cutting")
    do_cut(cur_data, "houghPeakness", lambda x: x>=7)
    do_cut(cur_data, "houghDistance", lambda x: x<=100)
    do_cut(cur_data, "houghCleaningPercentage", lambda x: x>=0.7)
    do_cut(cur_data, "octantsHit", lambda x: x>=5)

    cur_events = len(cur_data["EventNum"])
    print("Surviving Events:",  cur_events)

    print("start writing")
    for key in keys:
        dataset = outgroup.get(key)
        dataset.resize(surviving_events + cur_events, axis=0)
        if len(dataset.shape) > 1:
            dataset[surviving_events:,:] = cur_data[key]
        else:
            dataset[surviving_events:] = cur_data[key]
    surviving_events += cur_events
print("#Events that survived cutting:", surviving_events)
