#! /usr/bin/env python2
# -*- coding:utf8 -*-
"""
this file writes the delays in the inputfile
into a .csv readable by facttools

Usage:
    WriteDelayCsv.py <inputfile> [--inputgroup=<group>] [--key=<key>]

Options:
    --inputgroup=<group>    The hdf5 group  with the delays [default: results]
    --key=<key>             The hdf5 key  with the delays [default: delay]
"""

import numpy as np
import h5py
from docopt import docopt

args = docopt(__doc__)

inputfile = h5py.File(args["<inputfile>"], "r")

delays = np.array(inputfile.get(args["--inputgroup"] + "/" + args["--key"]))

np.savetxt("data/delays_20141114.csv",
           delays,
           fmt="%1.5f",
           header="delays in slices for each pixel, sorted by chid, must be ADDED to the arrivaltime")
