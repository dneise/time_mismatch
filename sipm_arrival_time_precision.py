import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
plt.ion()


class DRSchips(object):

    def __init__(self, shape):
        pass


def DRS4_sampling_widths(
        nominal_width=0.5,
        s_curve_effect=0.1,
        alternating_effect=0.1,
        alternating_variation=0.1,
        N=1024):
    """ This function return *one* possible set of DRS5 sample widths.
        It simply a wild guess and may be seen as my (D.Neises) personal
        opinion about how a DRS4 sample width distribution might look.

        The idea behind this is the following:
        1.) in 0th order all sample width are equal to the nominal width
        2.) the cumulative sum of the widths sometimes resembles a S-shaped curve
            Similr to a sine wave, so I add a cosine form to the sample widths
            With a period of N(=1024) and an amplitude that a few percent
            of the nominal width. (I start with 10%)
        3.) From Fig 9 in the Ritt paper [ref!] I learned that the widths of consequtive
            cells can vary a lot. It really looks like odd cells have large widths and even
            cells have very short width, or the other way around...
            So add some such a "very high freuency" noise.
            I call that 'alternating'.
            The overall size of the alternating effect can also be chosen
            as fractions of the nominal width. According to that paper it can be as high as 100%.
            So some cells really have zero width or even negative width.
            In addition I multiply a small variation to the alternating stuff.
            This should make it look more real. I multiply with a normally
            distributed random number, with mean=1. and sigma=alternating_variation.

        4.) Since a lot of random variations have been added to the
            widths, in the end I make sure the sum is still equal to N*nominal_width
            By scalling all widths accordingly.
    """
    i = np.arange(N)

    widths = np.ones(N) * nominal_width

    widths += np.cos(i * 2*np.pi / N) * nominal_width * s_curve_effect

    alternating = np.sin(np.pi*(i+1./2)) * nominal_width * alternating_effect
    alternating *= np.random.normal(1, alternating_variation, alternating.shape)
    widths += alternating

    # just to make sure the sum of all widths is equal to ROI*nominal_width.
    widths -= (widths-nominal_width).mean()

    return widths


class DRS4_sampling_times_new(object):

    def __init__(
            self,
            sampling_widths=np.ones((1440, 1024))*0.5):
        self.npix = sampling_widths.shape[0]
        self.nsamples = sampling_widths.shape[1]
        self.sampling_times = np.zeros((self.npix, 2*self.nsamples+1))
        self.sampling_times[:, 1:self.nsamples+1] = sampling_widths
        self.sampling_times[:, self.nsamples+1:] = sampling_widths
        self.sampling_times = self.sampling_times.cumsum(axis=1)

        #self.sampling_times = np.zeros((self.npix, 2*self.nsamples))
        #self.sampling_times[:, :self.nsamples] = sampling_widths
        #self.sampling_times[:, self.nsamples:] = sampling_widths
        #self.sampling_times = self.sampling_times.cumsum(axis=1)

    def __call__(
            self,
            stopcells=np.arange(1440) % 1024):
        stopcells += 1
        tmp = np.zeros((self.npix, self.nsamples))
        for i in range(self.npix):
            tmp[i] = self.sampling_times[i, stopcells[i]:stopcells[i]+self.nsamples] - self.sampling_times[i, stopcells[i]-1]
        return tmp


class DRS4_sampling_times_old(object):

    def __init__(
            self,
            sampling_widths=np.ones((1440, 1024))*0.5):
        self.sampling_widths = sampling_widths

    def __call__(
            self,
            stopcells=np.arange(1440) % 1024):
        """ creates sampling time array, based on the stopcells.
        """
        sampling_times = np.zeros_like(self.sampling_widths)
        for i in range(self.sampling_widths.shape[0]):
            sampling_times[i] = np.roll(self.sampling_widths[i], stopcells[i]).cumsum()
        return sampling_times


DRS4_sampling_times = DRS4_sampling_times_new


def generate_N_sampling_widths(
        s_curve_effects=np.linspace(-0.2, 0.2, 1440),
        N_samples=1024,
        nominal_width=0.5,
        alternating_effect=0.1,
        alternating_variation=0.1,
        ):
    N = len(s_curve_effects)
    a = np.zeros((N, N_samples), dtype=np.float32)
    for i, s in enumerate(s_curve_effects):
        a[i] = DRS4_sampling_widths(
            nominal_width=nominal_width,
            s_curve_effect=s,
            alternating_effect=alternating_effect,
            alternating_variation=alternating_variation,
            N=N_samples,
            )
    return a


def FWHM(h, bins):
    bin_centers = (bins[1:]+bins[:-1])/2.
    spline = UnivariateSpline(bin_centers, h-np.max(h)/2., s=0)
    r1, r2 = spline.roots()
    return r2-r1


def find_stopcells(sampling_widths, trigger_time):
    period = sampling_widths[0].sum()
    # N_samples = sampling_widths.shape[1]
    trigger_time = trigger_time % period
    stopcells = np.zeros(sampling_widths.shape[0], dtype=np.int16)
    for n in range(sampling_widths.shape[0]):
        #~ print stopcells[n].shape, sampling_widths[n].cumsum().shape, trigger_time
        #~ print sampling_widths[n].cumsum()
        #~ print np.digitize( sampling_widths[n].cumsum(), trigger_time)
        stopcells[n] = np.digitize([trigger_time], sampling_widths[n].cumsum())
    return stopcells


def digitize(y, bins=np.arange(-1024, 1024, 0.5)):
    return bins[np.digitize(y, bins=bins+0.25)]


def pulse(t, t0=0.):
    y = np.zeros_like(t)
    #y[t >= t0] = 10 * 1.626*(1-np.exp(-0.3803*0.5*(t[t >= t0]-t0))) * np.exp(-0.0649*0.5*(t[t >= t0]-t0))
    y[t >= t0] = 10 * 1.626*(1-np.exp(-0.3803*(t[t >= t0]-t0))) * np.exp(-0.0649*(t[t >= t0]-t0))
    return y


def FindArrivalTime(y, t):
    maxpos = MaxAmplitudePosition(y)
    max_slope_pos = RisingEdgeForPositions(y, maxpos, window_left=25, width=5)
    found_time = t[max_slope_pos]
    return found_time


def MaxAmplitudePosition(y, window_left=25, window_right=125):
    """ find maximum position, like fact-tools.features.MaxAmplitudePosition.java
        using np.argmax()

        Note: There is a wrong comment in MaxAmplitudePosition.java
            That says: "If the maximum value is not unique the last position will be used."
            But this comment does not fit to the code.
    """
    if window_left is None:
        return y[:window_right].argmax()
    else:
        return window_left + y[window_left:window_right].argmax()


def RisingEdgeForPositions(y, maxpos, window_left=25, width=5):
    """
    find position of largest slope (over 5 samples) like  fact-tools.features.RisingEdgeForPositions
    in a window from maxpos-window_left to maxpos.
    width should be an odd integer.
    """
    half_width = int(width // 2)
    if half_width*2 == width:
        print "Warning in RisingEdgeForPositions: width is not an odd integer, using half_width=", half_width

    y_right = y[maxpos+half_width-window_left : maxpos+half_width]
    y_left = y[maxpos-half_width-window_left : maxpos-half_width]

    slopes = y_right - y_left
    return slopes.argmax() + maxpos - window_left


def measure_precision(plot=True):
    """ I try to measure the typcial precision of the arrival time
        extraction here. Currently(28.07.14) I am looking at the method used in
        the muon analysis of Max Noethe. But this might change.
        He used fact-tools, so the algorithms he used have be reimplemented.

        The precision usually depends on the size of the pulse compared to
        the noise. So I am looking at different pulse sizes, i.e. numbers
        of photons, *N*.

        For each N we collect the stddev of the arrival time distribution
        in a list.

        the results are two np.arrays:
            Ns      : array of Ns
            stddevs : array of

    """
    # typical FACT event length 150ns with 0.5ns sampling
    t = np.arange(0, 150, 0.5)

    # typically in FACT the trigger time is between 20 and 30ns.
    true_times = np.random.uniform(20, 30, 1000)

    Ns = np.arange(2, 10, 0.5)
    stddev_as_function_of_N = []

    for N in Ns:
        found_times = []
        for t0 in true_times:
            y = N*pulse(t, t0)
            y += np.random.normal(0, 2.0, y.shape)

            maxpos = MaxAmplitudePosition(y)
            max_slope_pos = RisingEdgeForPositions(y, maxpos)
            found_time = t[max_slope_pos]
            found_times.append(found_time)

            #~ print t0, found_time, t[maxpos]
            #~ plt.plot(t,y,'.:' , hold=False)
            #~ raw_input('?')

        found_times = np.array(found_times)
        diff = true_times-found_times

        print "N=", N, "diff: mean=", diff.mean(), "stddev:", diff.std()
        stddev_as_function_of_N.append(diff.std())

    stddev_as_function_of_N = np.array(stddev_as_function_of_N)
    plt.ion()
    plt.figure()
    plt.errorbar(Ns, stddev_as_function_of_N, yerr=stddev_as_function_of_N/np.sqrt(2*(len(true_times)-1)), fmt='.:')
    #~ (x, y, yerr=yerr, fmt='o')

    return Ns, stddev_as_function_of_N


def random_reflector_time_spread(N, timespread_devisor=1.):
    if N < 1:
        return np.array([])
    r = np.random.uniform(0, 1, N)

    bin_edges = np.array([0.27, 0.51, 0.77, 1])  # %

    values = np.array([0, 0.42, 0.56, 0.82])  # ns
    values /= timespread_devisor
    r = values[np.digitize(r, bins=bin_edges)]
    r += np.random.normal(0, 0.005, r.shape)
    return r


def time_reconstruction_accuracy(plot=False):
    t = np.arange(0, 150, 0.5)
    noise_levels = np.arange(0.25, 4, 0.75)
    n_photons = np.arange(1, 20, 0.5)
    means = np.zeros((len(n_photons), len(noise_levels)))
    stds = np.zeros((len(n_photons), len(noise_levels)))
    N = 1000
    if plot:
        plt.ion()
        fig = plt.figure()
        fig2 = plt.figure()
        ax = fig.add_subplot(111)
        ax2 = fig2.add_subplot(111)

    for i_n, nl in enumerate(noise_levels):
        for ip, nphot in enumerate(n_photons):
            true_time = np.random.normal(40, 3, N)
            found_times = np.zeros_like(true_time)
            for e_id in range(N):
                y = nphot*pulse(t, true_time[e_id])
                y += np.random.normal(0, nl, y.shape)
                y = digitize(y)
                maxpos = MaxAmplitudePosition(y)
                max_slope_pos = RisingEdgeForPositions(y, maxpos)
                found_times[e_id] = t[max_slope_pos]
            diffs = found_times - true_time
            means[ip, i_n] = diffs.mean()
            stds[ip, i_n] = diffs.std()
            # print nphot, nl, diffs.mean(), diffs.std()
        ax.semilogy(n_photons, stds[:, i_n], '.:', label="noise:{}".format(nl))
        ax2.errorbar(n_photons, means[:, i_n], yerr=stds[:, i_n], fmt='.:', label="noise:{}".format(nl))
        plt.figure(fig.number)
        plt.legend()
        plt.xlabel("number of photons")
        plt.ylabel("$\sigma_{at}$[ns]")
        plt.title("performance of RisingEdgeForPositions")
        plt.figure(fig2.number)
        plt.legend()
        fig.show()
        fig2.show()

    return noise_levels, n_photons, means, stds


def play_with_time_recontruction(
        plot=False,
        noise_level=2.0,
        delta_ts=np.arange(0., 20., 0.025)
        ):
    """ Look at the arrival time reconstruction algorithms perfomrance.

    In order to learn more about the interplay of MaxAmplitudePosition and
    RisingEdgeForPositions in connection with two very near pulses and some
    electronics noise, we simulate it.

    double pulses are  generated in one pixel. The arrival time difference dt
    of the two pulses varies between 0. and 2. ns. in steps of 0.1ns.

    The pulses are sampled with 2GHz on 150ns. The arrival time of the
    first pulse is not fixed, but varies between 20. and  30ns.

    In order to generate some statistics 1000 of these events are simulated and
    the arrival time(whatever that means) is reconstructed.  If **plot**
    if set to True, one can look at one example of the 1000 events, for each
    dt.

    """

    t = np.arange(0, 150, 0.5)
    true_times = np.random.uniform(20, 30, 2000)
    rec_ts = []
    rec_ts_err = []

    if plot:
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)
        ax.set_title("play_with_time_recontruction - debug plot")
        ax2.set_title("play_with_time_recontruction - arrival times")

    for dt in delta_ts:
        found_times = []
        for t0 in true_times:
            y1 = 1*pulse(t, t0)
            y2 = 1*pulse(t, t0 + dt)
            y = y1+y2
            y += np.random.normal(0, noise_level, y.shape)
            maxpos = MaxAmplitudePosition(y)
            max_slope_pos = RisingEdgeForPositions(y, maxpos, window_left=25, width=5)
            found_time = t[max_slope_pos]
            found_times.append(found_time - t0)

        found_times = np.array(found_times)
        rec_ts.append(found_times.mean())
        rec_ts_err.append(found_times.std())
        if plot:
            ax.cla()
            ax.plot(t, y, 'b.:', label="data")
            ax.plot(t[2:], y[2:]-y[:-2], 'g.:', label="derivative")
            ax.plot([t0, t0], [0, 20], 'm-', lw=2, label="arrival time 1st")
            ax.plot([t0+dt, t0+dt], [0, 20], 'm-', lw=2, label="arrival time 2nd")
            ax.plot([found_time, found_time], [0, 20], 'r-', lw=2, label="arrival time 2nd")
            ax2.cla()
            ax2.errorbar(delta_ts[:len(rec_ts)], rec_ts, yerr=rec_ts_err, fmt='.:')
            ax2.set_xlabel('delta_ts')
            ax2.set_ylabel('rec_ts')
            fig.show()
            # raw_input('?')

    print np.polyfit(delta_ts, rec_ts, 1)
    return delta_ts, rec_ts


def arrival_times_on_ring(
        noise_sigma=1.0,
        timespread_devisor=1.,
        sampling_widths=np.ones((1440, 1024))*0.5,
        pixel_delays=np.zeros(1440),
        N_events=500,
        plot=False,
        ppp=5,
        pixel_in_ring=20,
        ):
    """ simulates *N_events* of muon events and measure arrival time distribution

    For each event, the number of pixels in the "ring" is drawn from a posision
    with expectation *pixel_in_ring*. For each pixel the number of photons which
    hit this pixel is drawn from a poisson with expectation *ppp* (photons per pixel).
    The arrival time of each photon is incidentally equal to 30., but is spread out
    by the arrival time distribution from random_reflector_time_spread().
    The *timespread_devisor* defines if the reflector is a typical Davies Cotton
    configuration (1.) or a perfect isochronous reflector (np.inf). The current
    FACT reflector is a hybrid, which can be defined by timespread_devisor=2.

    White electronics noise with sigma=*noise_sigma* is added prior to signal
    digitization. The arrival time is found by FindArrivalTime()

    Return values: 3 arrays of length *N_events*
        * number of pixels  hit in each event
        * width of arrival time distribution  of each event
        * mean of arrival time distribution of each event.
    """

    if sampling_widths is None:
        sampling_widths = np.ones((1440, 1024))*0.5

    period = sampling_widths[0].sum()

    stddevs = []
    means = []
    pixel_per_event = np.random.poisson(pixel_in_ring, N_events)

    trigger_times = np.linspace(0, period, N_events)

    # loop over all events
    sampling_times_gen = DRS4_sampling_times(sampling_widths)
    for eid in range(N_events):
        npix = pixel_per_event[eid]
        trigger_time = trigger_times[eid]
        stopcells = find_stopcells(sampling_widths, trigger_time)
        sampling_times = sampling_times_gen(stopcells)

        arrival_times = np.zeros(npix)
        photons_per_pixel = np.random.poisson(ppp, npix)

        # np.random.choice is only available in numpy 1.7.0
        #~ pixel_ids = np.random.choice(np.arange(1440), npix, replace=False)
        # we use shuffle instead
        pixel_ids =np.arange(1440)
        np.random.shuffle(pixel_ids)
        pixel_ids = pixel_ids[:npix]

        # loop over all pixels in an event
        for i in range(npix):
            p = photons_per_pixel[i]
            pi = pixel_ids[i]
            # t is an array of sampling times ... in a perfect world it would be:
            # t = np.arange(0, 150, 0.5)
            # but now we need to 'generate' sampling times according to the aperture
            # time jitter of simulated DRS4 channels.
            t = sampling_times[pi]

            true_arrival_times = random_reflector_time_spread(p, timespread_devisor)+30
            y = np.zeros_like(t)
            for t0 in true_arrival_times:
                y += pulse(t, t0)
            y += np.random.normal(0, noise_sigma, y.shape)
            y = digitize(y)
            try:
                found_time = FindArrivalTime(y, t)
                arrival_times[i] = found_time + pixel_delays[pi]
            except Exception as e:
                arrival_times[i] = -1.
                print e

        stddevs.append(arrival_times.std())
        means.append(arrival_times.mean())

    if plot:
        plt.figure()
        plt.ion()
        plt.hist2d(stddevs, pixel_per_event, bins=[100, 25], range=[[0, 4], [0, 50]])
        plt.colorbar()

    return pixel_per_event, np.array(stddevs), np.array(means)


def arrival_times_on_ring2(
        noise_sigma=1.0,
        timespread_devisor=1.,
        sampling_widths=np.ones((1440, 1024))*0.5,
        N_events=100,
        pixel_delays=np.zeros(1440)):
    """ Estimate the std-dev of the "arrival times" of the photon pulses in
        a couple of pixels in the most tyical muon event.

        We simulate *N_events* just in order to be able to generate some
        statistics. Each event tries to be the most typical muon even anyway,
        so actually we would not need statistics.
        We do not simulate large outliers anyway...
    """
    period = sampling_widths[0].sum()

    trigger_times = np.linspace(0, period, N_events)
    all_arrival_times = []
    all_true_arrival_times = []

    sampling_times_gen = DRS4_sampling_times(sampling_widths)

    for ei, trigger_time in enumerate(trigger_times):
        stopcells = find_stopcells(sampling_widths, trigger_time)
        sampling_times = sampling_times_gen(stopcells)

        arrival_times = []
        event_true_arrival_times = []

        # We randomly find the number of photons hitting each pixel
        # Only pixels with more than 6 photons are taken into account.
        # The mean number of pixels with more than 6 photons is ~40
        # And the mean size(=sum of all numbers of photons) of these events is
        # is around 340.
        N_photons = np.random.poisson(5.5, 130)
        N_photons = N_photons[N_photons > 6]

        pixel_ids = np.random.choice(np.arange(1440), len(N_photons), replace=False)

        for i, pi in enumerate(pixel_ids):
            N = N_photons[i]

            # t is an array of sampling times ... in a perfect world it would be:
            # t = np.arange(0, 150, 0.5)
            # but now we need to 'generate' sampling times according to the aperture
            # time jitter of simulated DRS4 channels.
            t = sampling_times[pi]

            true_arrival_times = random_reflector_time_spread(N, timespread_devisor) + 25.
            event_true_arrival_times.append(true_arrival_times)
            y = np.zeros_like(t)
            for t0 in true_arrival_times:
                y += pulse(t, t0)

            y += np.random.normal(0, noise_sigma, y.shape)
            try:
                maxpos = MaxAmplitudePosition(y)
                max_slope_pos = RisingEdgeForPositions(y, maxpos, window_left=25, width=5)
                found_time = t[max_slope_pos] + pixel_delays[pi]
                arrival_times.append(found_time)
            except Exception as e:
                print e

        arrival_times = np.array(arrival_times)
        all_arrival_times.append(arrival_times)
        all_true_arrival_times.append(event_true_arrival_times)

    return all_arrival_times, all_true_arrival_times


def find_elec_noise_level():
    elec_noise_levels = np.arange(1., 3., 0.25)
    timespread_devisor = 1.
    stuff = []
    for noise_sigma in elec_noise_levels:
        N_pixel, stddevs, means = arrival_times_on_ring(
            noise_sigma,
            timespread_devisor,
            N_events=300)
        stuff.append((N_pixel, stddevs, means))

    medians = np.zeros(len(stuff))
    fwhms = np.zeros(len(stuff))
    for i, things in enumerate(stuff):
        stddevs = things[1]
        medians[i] = np.median(stddevs)
        # h, b = np.histogram(stddevs, bins=100, range=[0, 4])
        # fwhms[i] = FWHM(h, b)

    plt.figure()
    # plt.errorbar(elec_noise_levels, medians, yerr=fwhms, fmt='o')
    plt.errorbar(elec_noise_levels, medians, yerr=fwhms, fmt='o')
    plt.xlabel("simulated electronics noise [mV]")
    plt.ylabel("median of distribution of arrival time widths [ns]")
    plt.title("Finding the right electronics noise level...")
    plt.tight_layout()
    plt.grid()
    plt.savefig("arrival_width_vs_elec_noise.png", dpi=100)

    return stuff


def stddevs(plot=False):
    if plot:
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        #~ colors = ['r', 'b', 'g', '#FFBB00']

    elec_noise_levels = [1.5]
    timespread_devisor = 1.
    stuff = []
    for noise_sigma in elec_noise_levels:
        things = arrival_times_on_ring(
            noise_sigma,
            timespread_devisor,
            N_events=300)
        N_pixel, stddevs, means = things
        print "noise level:{0}, median:{1}, FWHM:{2}".format(
            noise_sigma, np.median(stddevs), stddevs.std())
        stuff.append(things)

        if plot:
            ax.hist(
                stddevs,
                bins=60,
                range=[0, 4],
                histtype='step',
                #~ alpha=0.8, label="Mirror: {} Noise: {}".format(timespread_devisor, noise_sigma),
                alpha=0.8,
                label="Noise: {}".format(noise_sigma),
                #~ color=color,
                linewidth=2,
                log=True)

    if plot:
        ax.set_ylabel("#")
        ax.set_xlabel("$\sigma_t/$ns")
        ax.set_xlim(0, 4)
        ax.legend(loc='best')
        ax.set_xticks(np.arange(0, 4.1, 0.25))
        ax.grid()
        fig.tight_layout()
        # fig.savefig("bla.png", dpi=100)
        plt.show()

    return stuff


def simple_smear_out_experiment(plot=False):
    timespread_devisor = 1.
    noise_sigma = 1.5
    sampling_widths_list = [
        np.ones((1440, 1024))*0.5,
        generate_N_sampling_widths(np.zeros(1440), 1024),
        generate_N_sampling_widths(np.linspace(-0.2, 0.2, 1440), 1024),
        generate_N_sampling_widths(np.linspace(-0.5, 0.5, 1440), 1024)]

    if plot:
        fig = plt.figure()
        fig2 = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        #~ colors = ['r', 'b', 'g', '#FFBB00']

    stuff = []
    for i in range(len(sampling_widths_list)):
        if plot:
            plt.figure(fig2.number)
            plt.subplot(len(sampling_widths_list), 1, i+1)
            plt.imshow(
                (sampling_widths_list[i]-0.5).cumsum(axis=1),
                interpolation='nearest',
                aspect='auto')
            plt.colorbar()

        things = arrival_times_on_ring(
            noise_sigma,
            timespread_devisor,
            N_events=300,
            sampling_widths=sampling_widths_list[i],
            )
        N_pixel, stddevs, means = things

        print "noise level:{0}, median:{1}, stddev:{2}".format(
            noise_sigma, np.median(stddevs), stddevs.std())
        print sampling_widths_list[i]
        stuff.append(things)

        if plot:
            ax.hist(stddevs, bins=60, range=[0, 4], histtype='step',
                    #~ alpha=0.8, label="Mirror: {} Noise: {}".format(timespread_devisor, noise_sigma),
                    alpha=0.8, label="sampling_widths:".format(noise_sigma),
                    #~ color=color,
                    linewidth=2, log=True)

    if plot:
        ax.set_ylabel("#")
        ax.set_xlabel("$\sigma_t/$ns")
        ax.set_xlim(0, 4)
        ax.legend(loc='best')
        ax.set_xticks(np.arange(0, 4.1, 0.25))
        ax.grid()
        fig.tight_layout()
        # fig.savefig("bla.png", dpi=100)
        plt.show()

    return stuff


def patch_delay(
        how_many=160/8,
        amplitude=4,
        plot=True,
        ):
    patch_delays = np.zeros(160)
    patch_delays[:how_many] = np.linspace(-1*amplitude, amplitude, how_many)
    pixel_delays = patch_delays.repeat(9)

    s_curve = np.zeros(1440)
    sampling_widths = generate_N_sampling_widths(s_curve, 1024)
    timespread_devisor = 1.
    noise_sigma = 1.

    res = arrival_times_on_ring(
        noise_sigma,
        timespread_devisor,
        sampling_widths=sampling_widths,
        N_events=300,
        pixel_delays=pixel_delays)

    pixel_per_event, stddevs, means = res
    if plot:
        plt.figure()
        plt.hist(stddevs, bins=100, range=(0, 4), label="N={0}, A={1}".format(how_many, amplitude))
        plt.title("arrival time width distribution median={}".format(np.median(stddevs)))
        plt.xlabel("$\sigma_{at}$[ns]")
        plt.legend()
        plt.show()

    return res


def test_DRS4_sampling_times():
    old = DRS4_sampling_times_old()
    N = DRS4_sampling_times()
    new = N()
    return old, new


def bla(a=0.2):
    w = generate_N_sampling_widths(
        alternating_effect=0.,
        alternating_variation=1e-20,
        s_curve_effects=np.linspace(-1*a, a, 1440))
    w2 = np.concatenate((w, w), axis=1)
    t = w2.cumsum(axis=1)[:, :1024+60]
    shifts = t[:, 60:1024+60]-t[:, :1024]
    return shifts

if __name__ == '__main__':
    pass
