# -*- coding:utf8 -*-
"""
This  script is intended to test the calculation of the delays with toy monte carlo
data

There are still some problems with this script:
    The delays can only be calculated as a relative offset, that means there is
    a free choice of a global constant. It would make sense to calculate the delays such
    that their sum vanishes, but I wasn't able to create an LEQS with that constraint.
    Instead I calculated the delays relative to the pixel, I had the most information about.
    This is pixel 992 (hardcoded :-().
    So Pixel 992 always has a delay of zero.
    One can of course shift all delays such that their sum vanishes still afterwards
    but that is not done in this script.

    Also the runtime of the scipt is pretty long I think, and the fact that some
    stuff is still hardcoded is awful.

Improve it if you can.
"""
from __future__ import unicode_literals
import numpy as np
import scipy
from scipy.sparse import lil_matrix, coo_matrix, dok_matrix
import scipy.sparse.linalg as ssli
import time
import h5py
import matplotlib.pyplot as plt

infile = 'build/toy_mc.hdf5'

def LSQR_matrix(
        arrival_times,
        ringpixel):
    """
    arrival_times : np.array, 1440 float, 1D: reconstructed arrival time per pixel
    ringpixel : np.array, some ints, 1D: chids of pixels to be used
    """
    at = arrival_times[:,:]
    rp = ringpixel[:,:]

    rhs = []
    left = []
    right = []

    for i in range(at.shape[0]):
        # print i, '/', at.shape[0]
        pixel_ids = rp[i].nonzero()[0]
        for i1 in range(len(pixel_ids)):
            for i2 in range(i1+1, len(pixel_ids)):
                p1 = pixel_ids[i1]
                p2 = pixel_ids[i2]
                delay = at[i, p1]-at[i, p2]
                rhs.append(delay)
                left.append(p1)
                right.append(p2)

    left = np.array(left)
    right = np.array(right)
    rhs = np.array(rhs)
    print time.asctime(), "making lil", (len(left), 1440)
    A = dok_matrix((len(left), 1440))

    print time.asctime(), "before filling lil"
    for i in range(len(left)):
        if i % 100000 == 0:
            print time.asctime(), i, '/', len(left)
        A[i, left[i]] = -1
        A[i, right[i]] = 1
    print time.asctime(), "filled lil"

    Acsr = A.tocsr()
    rhs = np.array(rhs)

    return Acsr, rhs

if __name__ == '__main__':
    print time.asctime(), "starting up .. reading input"

    f = h5py.File(infile, 'r')
    grp = f['mc_data']
    at = grp['arrivalTime']
    rp = grp['bestRingPixel']
    true_delay = grp['delays']


    # rp are those pixels, that survived the cleaning, with a level=2
    # we want to change the level, so we are left with less pixels.
    # plt.hist( np.nonzero(rp6)[1], bins=np.arange(1441), lw=0)

    print time.asctime(), "before making lil_matrix"
    A, rhs = LSQR_matrix(at, rp)
    print time.asctime(), "boot strapping"

    h, b = np.histogram(np.nonzero(rp)[1], bins=np.arange(1441))
    unknown_pixels = np.where(h == 0)[0]
    known_pixels = np.ones(1440, dtype=np.bool)
    known_pixels[unknown_pixels] = False
    highest_pixel = 992
    known_pixels[highest_pixel] = False

    N_bootstrap = 50
    all_delays = np.ones((N_bootstrap,1440), dtype=np.float32) * np.nan
    A_cut = A[:, known_pixels]

    for i in range(N_bootstrap):
        print time.asctime(), i, '/', N_bootstrap

        N = A.shape[0]
        matrix_rows = np.random.choice(np.arange(N), int(0.5*N))
        A_cut2 = A_cut[matrix_rows]
        res = ssli.lsqr(A_cut2, b=rhs[matrix_rows])

        all_delays[i, known_pixels] = res[0]
        all_delays[i, highest_pixel] = 0.

    print time.asctime(), "done"

    nanmeandelays = np.nanmean(all_delays, axis=0)
    NN = (~np.isnan(all_delays)).sum(axis=0)
    # calculate board mean delays to be used for unknown pixels
    board_means = np.zeros(40)
    for i in range(40):
        board_means[i] = np.nanmean(all_delays[:, i*36:(i+1)*36])

    pixel_delays = np.zeros(1440)
    pixel_delays[known_pixels] = nanmeandelays[known_pixels]
    pixel_delays[unknown_pixels] = board_means[unknown_pixels/36]

    f = h5py.File(infile.replace(".hdf5", "_delays.hdf5"), 'w')
    grp = f.create_group("results")
    grp.create_dataset("delay", data=nanmeandelays)
    grp.create_dataset("delay_error", data=np.nanstd(all_delays, axis=0)/np.sqrt(NN))
    grp.create_dataset("number_of_hits_per_pixel", data=h)
    print time.asctime(), "results written closing file"
    f.close()

pixel_delays -= np.mean(pixel_delays)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.hist((pixel_delays + true_delay), 10)
ax.set_xlabel(r"$\text{delay} - \text{delay}_\text{wahr}$")
ax.set_ylabel("Häufigkeit")
fig.savefig("build/delay_error.pdf")
