import numpy as np
import matplotlib.pyplot as plt
import h5py

np.random.seed(0)
mean_pixel = 25
n_events = int(2e4)

time_noise = 0.5

# Draw delays for every Pixel
delays = np.random.normal(0, 1.5, 1440)

pixel = np.arange(1440)

arrivalTimes = np.zeros((n_events, 1440), dtype=int)
bool_ringpixel = np.zeros((n_events, 1440), dtype=bool)
std_devs_time = np.zeros(n_events)

for event in range(n_events):

    # Draw random number for signal pixel
    num_signal_pixel = np.random.poisson(mean_pixel)
    while num_signal_pixel > 200:
        num_signal_pixel = np.random.poisson(mean_pixel)

    # draw random signal pixel
    ringpixel = np.random.choice(pixel, num_signal_pixel)

    bool_ringpixel[event][ringpixel] = True
    arrivalTimes[event][ringpixel] = 50
    if time_noise != 0:
        arrivalTimes[event][ringpixel]+ np.random.normal(0, time_noise, num_signal_pixel)
    arrivalTimes[event] += delays
    std_devs_time[event] = np.std(arrivalTimes[event][ringpixel])

arrivalTimes = arrivalTimes.astype(int)

plt.hist(std_devs_time, 30, [0,4])
plt.show()

with h5py.File("build/toy_mc.hdf5", 'w') as f:
    grp = f.create_group("mc_data")
    grp.create_dataset("arrivalTime", data=arrivalTimes, compression="gzip")
    grp.create_dataset("bestRingPixel", data=bool_ringpixel, compression="gzip")
    grp.create_dataset("delays", data=delays, compression="gzip")
