#! /usr/bin/env python3
"""
Plot σ_at

Usage:
    PlotTimeDeviation.py [options]

Options:
    -s --save     saves the plot in ../Plots/Histo_StdDevTime6.pdf
    --png         use outputformat png instead of pdf
"""
import copy
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from collections import OrderedDict as OD
import os
import os.path
from docopt import docopt
import h5py


args = docopt(__doc__)

if args["--png"]:
    ending = "png"
else:
    ending="pdf"

if args["--save"] is True:
    figsize =(12/2.54, 8/2.54)
    plt.rcParams["font.size"] = 11
    plt.rcParams["legend.fontsize"] = 11
    outputpath = "Plots"
    if not os.path.exists(outputpath):
        os.makedirs(outputpath)
    save = True
else:
    figsize=(9,6)
    plt.rcParams["font.size"] = 20
    save = False

y = "#FFBF00"

def histo(data, data_key, xlabel, ylabel, figsize=figsize, bins=30, bin_limits=None, filename=None, legend_loc='best'):
    fig, ax = plt.subplots(figsize=figsize)
    histos = {}
    max_value = 0
    for key in data.keys():
        histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key)
        if max_value < max(histos[key][0]):
            max_value = max(histos[key][0])
            ax.set_ylim(0, 1.1*max_value)
    ax.grid()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend(loc=legend_loc)
    fig.tight_layout()
    if filename is not None:
        fig.savefig(os.path.join(outputpath, filename), dpi=300)
    return fig, ax, histos

def histo_errorbars(data, data_key, xlabel, ylabel, figsize=figsize, bins=30, bin_limits=None, filename=None, legend_loc='best', stat_key=None, shift=60):
    fig, ax = plt.subplots(figsize=figsize)
    histos = {}
    max_value = 0
    for key in data.keys():
        mask = np.logical_and(data[key][data_key]>=bin_limits[0], data[key][data_key]<=bin_limits[1])
        n_events_histo = len(data[key][data_key][mask])
        normalisation = 1/(n_events_histo*((bin_limits[1]-bin_limits[0])/bins))
        histos[key], bin_edges = np.histogram(data[key][data_key], bins, bin_limits)
        bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
        y_err = np.sqrt(histos[key])*normalisation
        ax.errorbar(bin_middles, histos[key]*normalisation, fmt=',', color=data[key]["color"], yerr=y_err)
        if stat_key is None:
            histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key)
        elif "Mean" in stat_key:
            histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key+" $\\bar{{x}}={:4.2f}$".format(data[key][stat_key].n))
        elif "Median" in stat_key:
            histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key+" $m={:4.2f}$".format(data[key][stat_key]))
        if max_value < max(histos[key][0]):
            max_value = max(histos[key][0])
            ax.set_ylim(0, 1.1*max_value)
    # ax.grid()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend(loc=legend_loc)
    # if stat_key is not None:
    #     for t in legend.get_texts():
    #         t.set_ha('right')
    #         t.set_position((shift,0))
    fig.tight_layout()
    if filename is not None and save:
        fig.savefig(os.path.join(outputpath, filename), dpi=300)

    return fig, ax, histos


print("start reading files")

muon_mc = {}
with h5py.File("data/muon_mc_20141115.hdf5", "r") as f:
    grp = f["facttools_output"]
    for key in grp.keys():
        muon_mc[key] = np.array(grp[key])

data = {}
with h5py.File("data/allphido_20141114.hdf5", "r") as f:
    grp = f["facttools_output"]
    for key in grp.keys():
        data[key] = np.array(grp[key])

data_nodrs = {}
with h5py.File("data/allphidofiles_201410_nodrs_cuts.hdf5", "r") as f:
    grp = f["cutted"]
    for key in grp.keys():
        data_nodrs[key] = np.array(grp[key])

data_corr = copy.deepcopy(data)
data_corr["StdDevTime6"] = data_corr["StdDevTime6Corr"]

data["numEvents"] = len(data["EventNum"])
muon_mc["numEvents"] = len(muon_mc["EventNum"])
data_nodrs["numEvents"] = len(data_nodrs["EventNum"])

print("DRS #Events=", data["numEvents"])
print("NoDRS #Events=", data_nodrs["numEvents"])
print("MuonMc #Events=", muon_mc["numEvents"])

print("finished reading")

# corr_data = copy.deepcopy(data)
# corr_data["arrivalTime"] += delays


data = OD((("NoDRS", data_nodrs),
           ("DRS", data),
           ("DelayCorr", data_corr),
           ("MMC", muon_mc)))

# print("start calculation")

# for key in data.keys():
#     data[key]["StdDevTime6"] = np.zeros(data[key]["numEvents"])
#     for event in range(data[key]["numEvents"]):
#         mask = np.logical_and(data[key]["photoncharge"][event]>=6, data[key]["bestRingPixel"][event])
#         signal_times = data[key]["arrivalTime"][event][mask]
#         data[key]["StdDevTime6"][event] = np.std(signal_times)

limits = [0,4]
bins = (limits[1]-limits[0])//4 * 30

for key in data.keys():
    mask = np.logical_and(data[key]["StdDevTime6"]>=limits[0], data[key]["StdDevTime6"]<=limits[1])
    data[key]["MedianStdDevTime6"] = np.median(data[key]["StdDevTime6"][mask])


print("finished calculation")
print("start plotting")

data["MMC"]["color"] = 'r'
data["DRS"]["color"] = 'g'
data["NoDRS"]["color"] = 'b'
data["DelayCorr"]["color"] = 'k'

fig_std_time, ax_std_time, histos_std_time = histo_errorbars(
    data, "StdDevTime6",
    r"$\sigma_\text{ArrivalTime}/\text{slices}$",
    r"$\text{Normierte Häufigkeit}$",
    bins=bins, bin_limits=limits,
    filename="Histo_StdDevTime6."+ending,
    stat_key="MedianStdDevTime6"
)
if save is not True:
    plt.show()

print("finished")
