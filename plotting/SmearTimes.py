#! /usr/bin/env python3
"""
Smear the arrival times according to the data monte carlo mismatch

Usage:
    PlotTimeDeviation.py [options]

Options:
    -s --save     saves the plots
"""
import copy
import matplotlib.pyplot as plt
import numpy as np
from GetBoolRingPixel import get_bool_ringpixel
from matplotlib.colors import LogNorm
from collections import OrderedDict as OD
import os
import os.path
from docopt import docopt
import h5py


args = docopt(__doc__)

if args["--save"] is True:
    figsize =(12/2.54, 8/2.54)
    plt.rcParams["font.size"] = 11
    plt.rcParams["legend.fontsize"] = 11
    outputpath = "./build"
    if not os.path.exists(outputpath):
        os.makedirs(outputpath)
    save = True
else:
    figsize=(9,6)
    plt.rcParams["font.size"] = 20
    save = False

y = "#FFBF00"

def apply_cut_ge(data_dict, cutkey, value):
    mask = data_dict[cutkey] >= value
    for key in data_dict.keys():
        data_dict[key] = data_dict[key][mask]

def apply_cut_le(data_dict, cutkey, value):
    mask = data_dict[cutkey] <= value
    for key in data_dict.keys():
        data_dict[key] = data_dict[key][mask]

def array_to_dict(array, keys):
    params = {}
    for key, param in zip(keys, array):
        params[key] = param
    return params

def histo(data, data_key, xlabel, ylabel, figsize=figsize, bins=30, bin_limits=None, filename=None, legend_loc='best'):
    fig, ax = plt.subplots(figsize=figsize)
    histos = {}
    max_value = 0
    for key in data.keys():
        histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key)
        if max_value < max(histos[key][0]):
            max_value = max(histos[key][0])
            ax.set_ylim(0, 1.1*max_value)
    ax.grid()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend(loc=legend_loc)
    fig.tight_layout()
    if filename is not None:
        fig.savefig(os.path.join(outputpath, filename), dpi=300)
    return fig, ax, histos

def histo_errorbars(data, data_key, xlabel, ylabel, figsize=figsize, bins=30, bin_limits=None, filename=None, legend_loc='best', stat_key=None, shift=60):
    fig, ax = plt.subplots(figsize=figsize)
    histos = {}
    max_value = 0
    for key in data.keys():
        mask = np.logical_and(data[key][data_key]>=bin_limits[0], data[key][data_key]<=bin_limits[1])
        n_events_histo = len(data[key][data_key][mask])
        normalisation = 1/(n_events_histo*((bin_limits[1]-bin_limits[0])/bins))
        histos[key], bin_edges = np.histogram(data[key][data_key], bins, bin_limits)
        bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
        y_err = np.sqrt(histos[key])*normalisation
        ax.errorbar(bin_middles, histos[key]*normalisation, fmt=',', color=data[key]["color"], yerr=y_err)
        if stat_key is None:
            histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key)
        elif "Mean" in stat_key:
            histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key+" $\\bar{{x}}={:4.2f}$".format(data[key][stat_key].n))
        elif "Median" in stat_key:
            histos[key] = ax.hist(data[key][data_key], bins, bin_limits, normed=True, histtype='step', color=data[key]["color"], label=key+" $m={:4.2f}$".format(data[key][stat_key]))
        if max_value < max(histos[key][0]):
            max_value = max(histos[key][0])
            ax.set_ylim(0, 1.1*max_value)
    # ax.grid()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend(loc=legend_loc)
    # if stat_key is not None:
    #     for t in legend.get_texts():
    #         t.set_ha('right')
    #         t.set_position((shift,0))
    fig.tight_layout()
    if filename is not None and save:
        fig.savefig(os.path.join(outputpath, filename), dpi=300)

    return fig, ax, histos


print("start reading files")

muon_mc = {}
with h5py.File("../data/muon_mc_20141028.hdf5", "r") as f:
    grp = f["facttools_output"]
    for key in grp.keys():
        muon_mc[key] = np.array(grp[key])

apply_cut_ge(muon_mc, "houghPeakness", 7.0)
apply_cut_ge(muon_mc, "houghCleaningPercentage", 0.7)
apply_cut_le(muon_mc, "houghDistance", 100)
apply_cut_ge(muon_mc, "octantsHit", 5)

new_psf = {}
with h5py.File("../data/new_psf.hdf5", "r") as f:
    grp = f["facttools_output"]
    for key in grp.keys():
        new_psf[key] = np.array(grp[key])

new_psf["numEvents"] = len(new_psf["EventNum"])
muon_mc["numEvents"] = len(muon_mc["EventNum"])

print("NewPSF #Events=", new_psf["numEvents"])
print("MuonMc #Events=", muon_mc["numEvents"])

print("finished reading")
print("start calculation")

smeared_muon_mc = copy.deepcopy(muon_mc)

data = OD((("NewPSF", new_psf),
           ("MC", muon_mc),
           ("Smeared MC", smeared_muon_mc)))


for key in data.keys():
    data[key]["StdDevTime6"] = np.zeros(data[key]["numEvents"])
    for event in range(data[key]["numEvents"]):
        mask = np.logical_and(data[key]["photoncharge"][event]>=6, data[key]["bestRingPixel"][event])
        signal_times = data[key]["arrivalTime"][event][mask]
        data[key]["StdDevTime6"][event] = np.std(signal_times)

for key in data.keys():
    mask = data[key]["StdDevTime6"] < 4
    data[key]["Median<4"] = np.median(data[key]["StdDevTime6"][mask])

delays = np.random.normal(0, np.sqrt(data["NewPSF"]["Median<4"]**2 - data["MC"]["Median<4"]**2), 1440)
for i, arr_time in enumerate(smeared_muon_mc["arrivalTime"]):
    smeared_muon_mc["arrivalTime"][i] = arr_time + delays

data["Smeared MC"]["StdDevTime6"] = np.zeros(data["Smeared MC"]["numEvents"])
for event in range(data["Smeared MC"]["numEvents"]):
    mask = np.logical_and(data["Smeared MC"]["photoncharge"][event]>=6, data["Smeared MC"]["bestRingPixel"][event])
    signal_times = data["Smeared MC"]["arrivalTime"][event][mask]
    data["Smeared MC"]["StdDevTime6"][event] = np.std(signal_times)

for key in data.keys():
    data[key]["MedianStdDevTime6"] = np.median(data[key]["StdDevTime6"])

print("finished calculation")
print("start plotting")

data["MC"]["color"] = 'r'
data["Smeared MC"]["color"] = 'm'
data["NewPSF"]["color"] = 'g'

fig_std_time, ax_std_time, histos_std_time = histo_errorbars(
    data, "StdDevTime6",
    r"$\sigma_\text{ArrivalTime}/\text{slices}$",
    r"$\text{Normierte Häufigkeit}$",
    bins=60, bin_limits=[0,8],
    filename="Histo_MC_Smeared.pdf",
    stat_key="MedianStdDevTime6"
)
if save is not True:
    plt.show()

print("finished")
