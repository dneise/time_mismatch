#! /usr/bin/env python3
"""
This file plots the classic EventView for every Event
in a data file. It need improvement, using command line argumetns
for the input file would be nice

Usage:
    FactEventPlotter <inputfile> <outputfile> [--group=<group>]

Options:
    --group=<group>   group from which to read, default: first found group
"""

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
import h5py
import os
import os.path
from docopt import docopt
from matplotlib.backends.backend_pdf import PdfPages

args = docopt(__doc__)

plt.rcParams["text.usetex"] = False

inputfile = h5py.File(args["<inputfile>"], "r")
if not args["--group"]:
    group = inputfile[list(inputfile.keys())[0]]
else:
    group = inputfile[args["--group"]]

photoncharge = np.array(group["photoncharge"])
nights = np.array(group["NIGHT"])
runids = np.array(group["RUNID"])
eventnums = np.array(group["EventNum"])
ringpixel = np.array(group["bestRingPixel"])

outputfolder = "build"
if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)


softID, chid, pixel_x_soft, pixel_y_soft = np.loadtxt("./data/pixel-map.csv",
                                                        skiprows=1,
                                                        delimiter=",",
                                                        usecols=(0,9,10,11),
                                                        unpack=True)

pixel_x_chid = np.zeros(1440)
pixel_y_chid = np.zeros(1440)

pixel_x_soft*=9.5
pixel_y_soft*=9.5

for i in range(1440):
    pixel_x_chid[chid[i]] = pixel_x_soft[i]
    pixel_y_chid[chid[i]] = pixel_y_soft[i]

size = 50



with PdfPages(args["<outputfile>"]) as pdf:
    for i, (pc, rp, eventnum) in enumerate(zip(photoncharge, ringpixel, eventnums)):
        fig = plt.figure(figsize=(5,4.5))
        ax = fig.add_subplot(1,1,1, aspect='equal')
        print("\rPlotting Event {:5d} of {:5d}".format(i, len(eventnums)), end="")
        plot = ax.scatter(pixel_x_chid,
                        pixel_y_chid,
                        c=pc,
                        vmin=0,
                        lw=1,
                        marker='H',
                        s=size,
                        cmap="gray")
        plot = ax.scatter(pixel_x_chid[rp],
                        pixel_y_chid[rp],
                        c=pc[rp],
                        vmin=0,
                        lw=0.5,
                        edgecolor='g',
                        marker='H',
                        s=size,
                        cmap="gray")
        ax.set_xticks([])
        ax.set_yticks([])
        fig.colorbar(plot, ax=ax, label="PhotonCharge / p.e.", shrink=0.9)
        ax.set_xlim(-200, 200)
        ax.set_ylim(-200, 200)
        ax.set_title("{}_{}, EventNum: {}".format(int(nights[i]), int(runids[i]), int(eventnums[i])))
        fig.tight_layout()
        pdf.savefig(fig)
        plt.close(fig)
        if i==50:
            break

    d = pdf.infodict()
    d['Title'] = 'Possible Muon-Events'
    d['Author'] = 'Max Nöthe'
print("\n done")
