#! /usr/bin/env python3
"""
This Script is intended to test if the PixelMap is
correct, it seems to have a few mistakes, some
pixel seem to be swapped maybe because of wrong hardwareID
in FACTMapV5a.txt
"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
import h5py
import os
import os.path
from matplotlib.backends.backend_pdf import PdfPages

plt.rcParams["text.usetex"] = False

outputfolder = "build"
if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)


softID, hardID,  pixel_x_soft, pixel_y_soft = np.genfromtxt("../data/FACTmapV5a.txt", usecols=(0,1, 8, 9), unpack=True)

pixel_x_soft *= 9.5
pixel_y_soft *= 9.5

crate = hardID // 1000
board = (hardID // 100) % 10
patch = (hardID // 10) % 10
pixel = (hardID % 10)

chid = pixel + 9 * patch + 36 * board + 360 * crate

pixel_x_chid = np.zeros(1440)
pixel_y_chid = np.zeros(1440)
patch_chid = np.zeros(1440)
plt.hist(hardID, 1440)
plt.show()

for i in range(1440):
    pixel_x_chid[chid[i]] = pixel_x_soft[i]
    pixel_y_chid[chid[i]] = pixel_y_soft[i]
    patch_chid[chid[i]] = patch[hardID[i]]

size = 50

fig = plt.figure(figsize=(5,4.5))
ax = fig.add_subplot(1,1,1, aspect='equal')
plot = ax.scatter(pixel_x_chid,
                  pixel_y_chid,
                c=patch,
                lw=1,
                cmap="jet",
                marker='H',
                s=size)
fig.colorbar(plot)
plt.show()
