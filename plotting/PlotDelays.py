#! /usr/bin/env python3
"""
This scripts plots the calculated delays.
One Plot shows the delays for each Pixel, one Plot the histogram
for the delays and a third one a camera view

Usage:
    PlotDelays.py <inputfile> [--group=group] [--outputpath=outputpath] [--png]

Options:
    --outputpath=outputpath      Path where the plots are save [default: Plots/]
    --group=group           The hdf5 group in which the delays are stored [default: results]
    --png             save as png instead of pdf
"""
import matplotlib.pyplot as plt
import numpy as np
import h5py
from docopt import docopt
import os
import os.path

args = docopt(__doc__)
outputpath = args["--outputpath"]
inputfile = args["<inputfile>"]

if args["--png"]:
    ending = "png"
else:
    ending = "pdf"

if not os.path.exists(outputpath):
    os.makedirs(outputpath)
    print("created output directory ", outputpath)


plt.rcParams["font.size"] = 10

f = h5py.File(inputfile, 'r')
grp = f.get(args["--group"])
delays = np.array(grp.get("delay"))
delays_err = np.array(grp.get("delay_error"))
# num_hits = np.array(f.get("results/num_hits"))
f.close()
print(delays)

valid_delays = np.logical_not(np.isnan(delays))

# Normlize delays to mean = 0
delays -= np.mean(delays[valid_delays])
print("Not known delays:", len(delays[np.isnan(delays)]))

# Plot delays vs chid

fig, ax = plt.subplots(1,1, figsize=(10,5))
ax.errorbar(np.arange(1440),
            delays,
            yerr=delays_err,
            fmt="r+",
            capsize=1,
            markersize=3
            )

ax.set_xticks(np.arange(0, 1441, 36))
ax.set_xticklabels([])
ax.set_xticks(np.arange(0, 1440, 36)+18, minor=True)
ax.set_xticklabels(np.arange(0, 40, 1), minor=True)
ax.set_xlim(0, 1440)
ax.set_xlabel("Board")
ax.set_ylabel("relative delay  /  slices")
ax.grid()
fig.tight_layout()
fig.savefig(os.path.join(outputpath, "PerPixelDelays."+ending), bbox_inches='tight')

# plot delay ni histogram
fig, ax = plt.subplots(1,1)
ax.hist(delays, bins=20, range=[-5,5], histtype='step')
ax.set_xlabel("delay  /  slices")
ax.set_ylabel("Anzahl Events")
fig.savefig(os.path.join(outputpath, "HistoDelays." + ending), bbox_inches='tight')

# plot camera view:

softID, chid, pixel_x_soft, pixel_y_soft = np.loadtxt("./data/pixel-map.csv",
                                                        skiprows=1,
                                                        delimiter=",",
                                                        usecols=(0,9,10,11),
                                                        unpack=True)

pixel_x_chid = np.zeros(1440)
pixel_y_chid = np.zeros(1440)

pixel_x_soft*=9.5
pixel_y_soft*=9.5

for i in range(1440):
    pixel_x_chid[chid[i]] = pixel_x_soft[i]
    pixel_y_chid[chid[i]] = pixel_y_soft[i]

size = 50


fig = plt.figure(figsize=(5,4.5))
ax = fig.add_subplot(1,1,1, aspect='equal')
plot = ax.scatter(pixel_x_chid,
                pixel_y_chid,
                c=delays,
                lw=1,
                marker='H',
                s=size,
                cmap="RdYlGn")
ax.scatter(pixel_x_chid[np.isnan(delays)],
                pixel_y_chid[np.isnan(delays)],
                c="black",
                lw=1,
                marker='H',
                s=size,
                cmap="RdYlGn")
ax.set_xticks([])
ax.set_yticks([])
fig.colorbar(plot, ax=ax, label="delay / slices", shrink=0.9)
ax.set_xlim(-200, 200)
ax.set_ylim(-200, 200)
fig.tight_layout()
fig.savefig(os.path.join(outputpath, "DelaysCameraView."+ending))
