# Monte-Carlo Timing Mismatch studies for FACT

## Data Files:
you should have a directory _data_ in the root directory of this repository,
containing the files in  __/fhgfs/groups/app/fact/data_analysis_output/muon_studies__

The easiest way is probably mounting the directory :

__sshfs phido:/fhgfs/groups/app/fact/data_analysis_output/muon_studies data__

where phido is your ssh hostname for phido